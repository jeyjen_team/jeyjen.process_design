export default {
  app:{
    start: 'start',
    init: '',
    stop: 'stop'
  },
  connection:{
    _open: 'connection_open',
    _close: 'connection_close'
  },
  project: {
    _put: 'put_project'
  },
  character:{
    _set_root: 'character_set_root',
    _select: 'character_select'
  },
  node:{
    _select: 'node_select',
    _set_root: 'node_set_root'
  }
}
