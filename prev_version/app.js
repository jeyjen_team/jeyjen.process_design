import 'react-grid-layout/css/styles.css';
import 'react-resizable/css/styles.css';
import './style/common.css';
import './style/style.css';
import React from 'react';
import Main from './app/main';

const App = () => (
    <Main/>
);
export default App;

